import { Component, OnInit } from "@angular/core";

import { Item } from "./item";
import { ItemService } from "./item.service";

import { Http } from "@angular/http";
import { TextField } from "ui/text-field";

@Component({
    selector: "ns-items",
    moduleId: module.id,
    templateUrl: "./items.component.html",
})
export class ItemsComponent implements OnInit {
    items: Item[];
    dataQuery = [];
    saveQueryForSearch = [];

    // This pattern makes use of Angular’s dependency injection implementation to inject an instance of the ItemService service into this class. 
    // Angular knows about this service because it is included in your app’s main NgModule, defined in app.module.ts.
    constructor(private itemService: ItemService) { }

    ngOnInit(): void {
        this.itemService.getData()
            .subscribe(responsed => {
                this.dataQuery = responsed;
                this.saveQueryForSearch = responsed
            },
            error => console.log(error)
        );
    }

    searchKeyword(search) {
        if(search){
            var filterData = [];
            for(var i = 0 ; i < this.saveQueryForSearch.length ; i++) {
                for (var key in this.saveQueryForSearch[i]) {
                    if(this.saveQueryForSearch[i][key].toString().indexOf(search)!=-1) {
                        filterData.push(this.saveQueryForSearch[i])
                    }
                }
            }
            this.dataQuery = filterData
            console.log('Query Succeed!')
        } else {
            console.log('Query Failed!')
        }
    }
}
const functions = require('firebase-functions');
const admin = require('firebase-admin')
const firebase = require('firebase')

var config = {
    apiKey: "AIzaSyBfkb3MEbRLppnuvAmkmYXhqXwX_ccrodM",
    authDomain: "midterm-fixed.firebaseapp.com",
    databaseURL: "https://midterm-fixed.firebaseio.com",
    projectId: "midterm-fixed",
    storageBucket: "midterm-fixed.appspot.com",
    messagingSenderId: "902989005113"
  };
  firebase.initializeApp(config);

exports.queryData = functions.https.onRequest((request, response) => {
 return firebase.database().ref('midterm').once('value').then(snapshot => {
     response.send(snapshot.val());
 });
});
